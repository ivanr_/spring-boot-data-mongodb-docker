package icom.config;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoTemplate;

@Configuration
@SuppressWarnings("unused")
public class AppConfig {

    @Bean
    public MongoClient mongoClient() {
        return MongoClients.create("mongodb://localhost:27017");
    }

    public @Bean
    MongoTemplate mongoTemplate() {

        ConcreteFigure redSquare = new ConcreteFigure(new Square(), new RedColour());

        return new MongoTemplate(mongoClient(), "mydatabase");
    }
}

interface Colour {
    String getColour();
}

class RedColour implements Colour {
    @Override
    public String getColour() {
        return "red";
    }
}

interface Shape {
    String getShape();
}

class Square implements Shape {
    @Override
    public String getShape() {
        return "square";
    }
}

class ConcreteFigure implements Colour, Shape{

    private final Shape shape;
    private final Colour colour;

    ConcreteFigure(Shape shape, Colour colour) {
        this.shape = shape;
        this.colour = colour;
    }

    @Override
    public String getColour() {
        return colour.getColour();
    }

    @Override
    public String getShape() {
        return shape.getShape();
    }
}