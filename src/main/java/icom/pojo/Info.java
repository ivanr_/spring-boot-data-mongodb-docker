package icom.pojo;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@SuppressWarnings({"FieldMayBeFinal", "unused"})
public class Info {
    @Id
    private String id;
    private String text;
    private long creationTime;

    public Info(String text) {
        this.text = text;
        this.creationTime = System.currentTimeMillis();
    }

    public String getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public long getCreationTime() {
        return creationTime;
    }

    @Override
    public String toString() {
        return "Info{" +
                "id='" + id + '\'' +
                ", text='" + text + '\'' +
                ", creationTime=" + creationTime +
                '}';
    }
}
