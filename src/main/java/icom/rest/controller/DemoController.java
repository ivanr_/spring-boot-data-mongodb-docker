package icom.rest.controller;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import icom.pojo.Info;
import icom.repository.QueryMongodbRepository;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.springframework.data.mongodb.core.query.Criteria.where;

@RestController
@RequestMapping("info")
@SuppressWarnings("unused")
public class DemoController {

    private static final String WEB_SPLITERATOR = "<br>";

    @Autowired
    private QueryMongodbRepository repository;

    @Autowired
    private MongoTemplate mongoTemplate;

    @GetMapping
    public String queryAll() {
        return StreamSupport
                .stream(repository.findAll().spliterator(), false)
                .map(info -> WEB_SPLITERATOR + info.toString())
                .collect(Collectors.toList())
                .toString();
    }

    @GetMapping("withMin")
    public String getAllMoreThan(@RequestParam(value = "min", required = false) String minValue) {
        return repository.findByCreationTimeGreaterThan(Long.parseLong(minValue)).toString();
    }

    @GetMapping("withMax/{max}")
    public String getAllLessThan(@PathVariable(value = "max") String maxValue) {
        return repository.findByCreationTimeLessThan(Long.parseLong(maxValue)).toString();
    }

    @PostMapping(consumes = "application/json")
    public String addInfo(@RequestBody LinkedHashMap<String, String> info, Model model) {
        Info savedEntity = repository.save(new Info(info.get("text")));
        return "entity was saved with id: " + savedEntity.getId();
    }

    @GetMapping("byField")
    public String getOne(@RequestParam(required = false) String fieldName, @RequestParam(required = false) String fieldValue) {
        validateFieldNameValue(fieldName, fieldValue);
        Info info = mongoTemplate.findOne(new Query(where(fieldName).is(fieldValue)), Info.class);
        return info == null ? "entity not found" : info.toString();
    }

    private void validateFieldNameValue(String... values) {
        if (Arrays.stream(values).anyMatch(Objects::isNull)) {
            throw new NullPointerException("please check fieldName and fieldValue parameters that must be not null");
        }
    }

    @PostMapping("postQuery")
    public String getByPostedNativeQuery(@RequestBody String query) {
//        QUERY EXAMPLE IS IN FILE queryExample.json
        return mongoTemplate.executeCommand(query).toString();
    }

    @GetMapping("byFieldWithCommand")
    public String getByFieldNotLimitedResult(@RequestParam(required = false) String fieldName,
                                             @RequestParam(required = false) String fieldValue) {
        validateFieldNameValue(fieldName, fieldValue);
        Document command = new Document();
        Document andSelectText = new Document();
        BasicDBList andList = new BasicDBList();
        andList.add(new BasicDBObject(fieldName, fieldValue));
        andSelectText.append("$and", andList);
        command.append("find", "info").append("filter", andSelectText);
        return mongoTemplate.executeCommand(command).toString();
    }

    @ExceptionHandler({NullPointerException.class})
    @ResponseStatus(value = HttpStatus.PRECONDITION_FAILED)
    public String proceedAfterNpe() {
        return "sorry but we got NullPointerException, please check the values you sent";
    }

    @ExceptionHandler(IllegalStateException.class)
    public String returnNumberFormatExceptionMessage() {
        return "format is incorrect";
    }
}
