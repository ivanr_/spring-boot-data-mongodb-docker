package icom.repository;

import icom.pojo.Info;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface QueryMongodbRepository extends CrudRepository<Info, String> {
    List<Info> findByCreationTimeLessThan(long maxCreationTime);

    List<Info> findByCreationTimeGreaterThan(long minCreationTime);
}
